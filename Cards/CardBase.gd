extends MarginContainer

onready var CardDatabase = preload("res://Assets/Cards/CardsDatabase.gd")
var Cardname: String = 'Mentor'
onready var CardInfo: Array = CardDatabase.DATA[CardDatabase.get(Cardname)]
onready var CardImg: String = str("res://Assets/Cards/", CardInfo[0], "/", Cardname, ".png")
onready var Orig_scale = rect_scale

var startpos = 0
var targetpos = 0
var startrot = 0
var targetrot = 0
var t = 0
var DRAWTIME = 1
var ORGANISETIME = 0.5
enum{
	InHand,
	InPlay,
	InMouse,
	FocusInHand,
	MoveDrawnCardToHand,
	ReOrganiseHand
}
var state = InHand
var setup = true
var startscale = Vector2()
var Cardpos = Vector2()
var ZoomInSize = 2
var ZOOMINTIME = 0.2
var ReorganiseNeighbours = true
var NumberCardsHand = 0
var Card_Numb = 0
var NeighbourCard
var Move_Neighbour_Card_Check = false

func _ready():
#	print(CardInfo)
	var CardSize = rect_size
	$Border.scale *= CardSize/$Border.texture.get_size()
	$Card.texture = load(CardImg)
	$Card.scale *= CardSize/$Card.texture.get_size()
	$CardBack.scale *= CardSize/$CardBack.texture.get_size()
	$Focus.rect_scale *= CardSize/$Focus.rect_size

	var Attack: String = str(CardInfo[1])
	var Retaliation: String = str(CardInfo[2])
	var Health: String = str(CardInfo[3])
	var Cost: String = str(CardInfo[4])
	var SpecialText = str(CardInfo[6])

	$Bars/TopBar/Name/CenterContainer/Name.text = Cardname
	$Bars/TopBar/Cost/CenterContainer/Cost.text = Cost
	$Bars/SpecialText/Text/CenterContainer/Type.text = SpecialText
	$Bars/BottomBar/Health/CenterContainer/Health.text = Health
	$"Bars/BottomBar/Attack/CenterContainer/A&R".text = str(Attack, '/', Retaliation)

func _physics_process(delta):
	match state:
		InHand:
			pass
		InPlay:
			pass
		InMouse:
			pass
		FocusInHand:
			if setup:
				Setup()
			if t <= 1:  # Alwails be a 1
				rect_position = startpos.linear_interpolate(targetpos, t)
				rect_rotation = startrot * (1-t)
				rect_scale = startscale * (1-t) + Orig_scale*2*t
				t += delta/float(ZOOMINTIME)
				if ReorganiseNeighbours:
					ReorganiseNeighbours = false
					NumberCardsHand = $'../../'.NumberCardsHand - 1
					if Card_Numb - 1 >= 0:
						Move_Neighbour_Card(Card_Numb - 1, true, 1)
					if Card_Numb - 2 >= 0:
						Move_Neighbour_Card(Card_Numb - 2, true, 0.25)
					if Card_Numb + 1 <= NumberCardsHand:
						Move_Neighbour_Card(Card_Numb + 1, false, 1)
					if Card_Numb + 2 <= NumberCardsHand:
						Move_Neighbour_Card(Card_Numb + 2, false, 0.25)
			else:
				rect_position = targetpos
				rect_scale = Orig_scale*2
				rect_rotation = 0
		MoveDrawnCardToHand: # animate from the deck to my hand
			if setup:
				Setup()
			if t <= 1:  # Alwails be a 1
				rect_position = startpos.linear_interpolate(targetpos, t)
				rect_rotation = startrot * (1-t) + targetrot*t
				rect_scale.x = Orig_scale.x * abs(2*t - 1)
				if $CardBack.visible:
					if t >= 0.5:
						$CardBack.visible = false
				t += delta/float(DRAWTIME)
			else:
				rect_position = targetpos
				state = InHand
				rect_rotation = targetrot
				t = 0
		ReOrganiseHand:
			if setup:
				Setup()
			if t <= 1:  # Alwails be a 1
				if Move_Neighbour_Card_Check:
					Move_Neighbour_Card_Check = false
				rect_position = startpos.linear_interpolate(targetpos, t)
				rect_rotation = startrot * (1-t) + targetrot*t
				rect_scale = startscale * (1-t) + Orig_scale*t
				t += delta/float(ORGANISETIME)
				if ReorganiseNeighbours == false:
					ReorganiseNeighbours = true
					if Card_Numb - 1 >= 0:
						Reset_Card(Card_Numb - 1)
					if Card_Numb - 2 >= 0:
						Reset_Card(Card_Numb - 2)
					if Card_Numb + 1 <= NumberCardsHand:
						Reset_Card(Card_Numb + 1)
					if Card_Numb + 2 <= NumberCardsHand:
						Reset_Card(Card_Numb + 2)
			else:
				rect_position = targetpos
				state = InHand
				rect_scale = Orig_scale
				rect_rotation = targetrot
#				t = 0

func Move_Neighbour_Card(Card_Numb, Left, SpreadFactor):
	NeighbourCard = $'../'.get_child(Card_Numb)
	if Left:
		NeighbourCard.targetpos = NeighbourCard.Cardpos - SpreadFactor*Vector2(65, 0)
	else:
		NeighbourCard.targetpos = NeighbourCard.Cardpos + SpreadFactor*Vector2(65, 0)
	NeighbourCard.setup = true
	NeighbourCard.state = ReOrganiseHand
	NeighbourCard.Move_Neighbour_Card_Check = true

func Reset_Card(Card_Numb):
#	if NeighbourCard.Move_Neighbour_Card_Check:
#		NeighbourCard.Move_Neighbour_Card_Check = false
#	else:
	NeighbourCard = $"../".get_child(Card_Numb)
	if not NeighbourCard.Move_Neighbour_Card_Check:
		NeighbourCard = $'../'.get_child(Card_Numb)
		if NeighbourCard.state != FocusInHand:
			NeighbourCard.state = ReOrganiseHand
			NeighbourCard.targetpos = NeighbourCard.Cardpos
			NeighbourCard.setup = true

func Setup():
	startpos = rect_position
	startrot = rect_rotation
	startscale = rect_scale
	t = 0
	setup = false

func _on_Focus_mouse_entered():
	match state:
		InHand, ReOrganiseHand:
			setup = true
			targetpos = Cardpos
			targetpos.y = get_viewport().size.y - $'../../'.CardSize.y*ZoomInSize
			state = FocusInHand
			

func _on_Focus_mouse_exited():
	match state:
		FocusInHand:
			setup = true
			targetpos = Cardpos
			state = ReOrganiseHand
